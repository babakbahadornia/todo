import { ThrowStmt } from '@angular/compiler';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TableComponent } from 'src/@core/base-components/tabel/table/table.component';
import { Id } from 'src/@core/base-components/table-checkbox/table-checkbox/table-checkbox.component';
import { ITableConfig } from 'src/@core/interfaces/ITableConfig.ineterface';
import { ITableOperation } from './../@core/interfaces/ITableOperation.interface';
import { BaseService } from './../@core/shared/services/base.service';
import { DeleteModalComponent } from './components/delete-modal/delete-modal/delete-modal.component';
import { EditModalComponent } from './components/edit-modal/edit-modal/edit-modal.component';
import { MakeDoneModalComponent } from './components/make-done-modal/make-done-modal/make-done-modal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'todo';
  todoForm!: FormGroup;
  dataSource: { id: number; title: string; status: string }[] = [];
  index = 1;
  @ViewChild('table', { static: true }) table!: TableComponent;

  tableConfig: ITableConfig = {
    headers: ['id', 'title', 'status', 'operations'],

    operations: [
      { icon: 'delete', color: 'red', action: (params) => this.delete(params) },
      { icon: 'edit', action: (params) => this.edit(params) },
    ],

    footerOperations: [
      {
        text: 'make done',
        color: 'primary',
        action: (params) => this.makeDone(params),
      },
    ],
  };

  constructor(private fb: FormBuilder, private baseService: BaseService) {}

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

    this.createForm();
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.

    this.todoForm
      .get('todo')
      ?.valueChanges.subscribe((data) => console.log(data));
  }

  createForm() {
    this.todoForm = this.fb.group({
      todo: [null, Validators.required],
    });
  }

  addTodo() {
    // this.dataSource = new Array();
    this.dataSource.push({
      id: this.index,
      title: this.todoForm.get('todo')?.value,
      status: 'In-Progress',
    });
    this.index++;
    this.table.setDataSource(this.dataSource);
  }

  edit(params: any) {
    const dialogRef = this.baseService.openModal<EditModalComponent, string>(
      EditModalComponent,
      {
        title: params.title,
      }
    );

    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.dataSource.forEach((item, index) => {
          if (params.id == item.id) {
            this.dataSource[index].title = data;
          }
        });
      }
      this.table.setDataSource(this.dataSource);
    });
  }
  delete(params: any) {
    const dialogRef = this.baseService.openModal<DeleteModalComponent, boolean>(
      DeleteModalComponent,
      {
        subject: params.title,
      }
    );

    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.dataSource.splice(this.dataSource.indexOf(params), 1);
      }
      this.table.setDataSource(this.dataSource);
    });
  }

  getSelectedRows(event: Id[]) {
    console.log(event);
  }

  makeDone(params: Id[]): void {
    const dialogRef = this.baseService.openModal<
      MakeDoneModalComponent,
      boolean
    >(MakeDoneModalComponent, {});

    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        params.forEach((item) => {
          this.dataSource.map((data) => {
            if (data.id == item) {
              data.status = 'Done';
            }
          });
        });
      }
      this.table.setDataSource(this.dataSource);
    });
  }
}
