import { NgModule } from '@angular/core';
import { DeleteModalModule } from './delete-modal/delete-modal.module';
import { EditModalModule } from './edit-modal/edit-modal.module';
import { MakeDoneModalModule } from './make-done-modal/make-done-modal.module';

@NgModule({
  declarations: [],
  imports: [],

  exports: [DeleteModalModule, EditModalModule, MakeDoneModalModule],
})
export class ComponentsModule {}
