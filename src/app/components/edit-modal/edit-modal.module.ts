import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/@core/shared/shared.module';
import { BaseComponentsModule } from 'src/@core/base-components/base-components.module';
import { EditModalComponent } from './edit-modal/edit-modal.component';

@NgModule({
  declarations: [EditModalComponent],
  imports: [SharedModule, BaseComponentsModule],
})
export class EditModalModule {}
