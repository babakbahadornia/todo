import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { SharedModule } from 'src/@core/shared/shared.module';
import { BaseComponentsModule } from 'src/@core/base-components/base-components.module';

@NgModule({
  declarations: [DeleteModalComponent],
  imports: [SharedModule, BaseComponentsModule],
  exports: [DeleteModalComponent],
})
export class DeleteModalModule {}
