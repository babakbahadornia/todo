import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-make-done-modal',
  templateUrl: './make-done-modal.component.html',
  styleUrls: ['./make-done-modal.component.scss'],
})
export class MakeDoneModalComponent implements OnInit {
  title = `Are you sure  to make selected todos done?`;

  constructor() {}

  ngOnInit(): void {}
}
