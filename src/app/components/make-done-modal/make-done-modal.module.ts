import { NgModule } from '@angular/core';
import { MakeDoneModalComponent } from './make-done-modal/make-done-modal.component';
import { SharedModule } from 'src/@core/shared/shared.module';
import { BaseComponentsModule } from 'src/@core/base-components/base-components.module';

@NgModule({
  declarations: [MakeDoneModalComponent],
  imports: [SharedModule, BaseComponentsModule],
  exports: [MakeDoneModalComponent],
})
export class MakeDoneModalModule {}
