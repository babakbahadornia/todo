export interface ITableOperation {
  action: (params: any) => void;
  color?: string;
  icon?: string;
  text?: string;
}
