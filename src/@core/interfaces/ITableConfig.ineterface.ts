import { ITableOperation } from './ITableOperation.interface';

export interface ITableConfig {
  headers: string[];
  operations?: ITableOperation[];
  footerOperations: ITableOperation[];
}
