import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [],
  exports: [
    AngularMaterialModule,
    CommonModule,
    AngularMaterialModule,
    ReactiveFormsModule,
  ],
})
export class SharedModule {}
