import { ComponentType } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root',
})
export class BaseService {
  constructor(private dialog: MatDialog) {}

  openModal<T, R>(
    component: ComponentType<T>,
    data: any,
    width = '50vw',
    height = '40vh',
    panelClass?: string
  ): MatDialogRef<T, R> {
    return this.dialog.open(component, {
      data: data,
      width: width,
      height: height,
      panelClass: panelClass,
    });
  }
}
