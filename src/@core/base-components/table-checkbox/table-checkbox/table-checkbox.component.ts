import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export type Id = number | string;
@Component({
  selector: 'app-table-checkbox',
  templateUrl: './table-checkbox.component.html',
  styleUrls: ['./table-checkbox.component.scss'],
})
export class TableCheckboxComponent implements OnInit {
  constructor() {}

  @Input()
  rowId: Id;

  @Output()
  emitId = new EventEmitter<ISelectedRow>();

  @Input()
  checked: boolean;

  ngOnInit(): void {}

  emitSelectedId(status: true | false) {
    this.emitId.emit({ id: this.rowId, status: status });
  }
}

export interface ISelectedRow {
  id: Id;
  status: boolean;
}
