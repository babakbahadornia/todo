import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableCheckboxComponent } from './table-checkbox/table-checkbox.component';
import { SharedModule } from 'src/@core/shared/shared.module';

@NgModule({
  declarations: [TableCheckboxComponent],
  imports: [SharedModule],
  exports: [TableCheckboxComponent],
})
export class TableCheckboxModule {}
