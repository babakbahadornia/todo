import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table/table.component';
import { SharedModule } from 'src/@core/shared/shared.module';
import { TextInputModule } from '../text-input/text-input.module';
import { TableCheckboxModule } from '../table-checkbox/table-checkbox.module';

@NgModule({
  declarations: [TableComponent],
  imports: [SharedModule, TextInputModule, TableCheckboxModule],
  exports: [TableComponent],
})
export class TabelModule {}
