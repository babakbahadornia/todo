import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChildren,
} from '@angular/core';
import { ITableConfig } from 'src/@core/interfaces/ITableConfig.ineterface';
import { ITodo } from 'src/@core/interfaces/ITodo.interface';
import {
  Id,
  ISelectedRow,
  TableCheckboxComponent,
} from '../../table-checkbox/table-checkbox/table-checkbox.component';

export interface PeriodicElement {
  title: string;
  id: number;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  displayedColumns: string[] = ['id', 'title'];

  dataSource: ITodo[] = [];

  @Input()
  config: ITableConfig;

  selectedRows: Id[] = [];

  @Output()
  selectedRowsEvent = new EventEmitter<(string | number)[]>();
  checked: boolean;
  headerChecked: boolean;

  constructor(private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {}

  getRowId(event: ISelectedRow) {
    if (event.status) {
      this.selectedRows.push(event.id);
    } else {
      this.selectedRows.splice(this.selectedRows.indexOf(event.id), 1);
    }

    this.selectedRowsEvent.emit(this.selectedRows);
  }

  getHeaderCheckedEvent(event: ISelectedRow) {
    if (event.status) {
      this.headerChecked = true;
      this.checked = true;
      this.dataSource.map((data) => this.selectedRows.push(data.id));
    } else {
      this.checked = false;
      this.selectedRows = [];
    }
  }

  setDataSource(data: any) {
    this.dataSource = data;

    if (this.headerChecked) {
      this.headerChecked = false;
      this.checked = false;
    }
  }
}
