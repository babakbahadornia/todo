import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextInputComponent } from './text-input/text-input.component';
import { SharedModule } from 'src/@core/shared/shared.module';

@NgModule({
  declarations: [TextInputComponent],
  imports: [SharedModule],
  exports: [TextInputComponent],
})
export class TextInputModule {}
