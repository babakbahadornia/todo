import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss'],
})
export class TextInputComponent implements OnInit {
  constructor() {}

  @Input()
  lablel: string;
  @Input()
  inputFormControlName: string;
  @Input()
  inputFormGroup: FormGroup;

  ngOnInit(): void {}
}
