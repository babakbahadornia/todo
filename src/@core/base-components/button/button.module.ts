import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { SharedModule } from 'src/@core/shared/shared.module';

@NgModule({
  declarations: [ButtonComponent],
  imports: [SharedModule],
  exports: [ButtonComponent],
})
export class ButtonModule {}
