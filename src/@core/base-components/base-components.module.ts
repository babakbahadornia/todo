import { NgModule } from '@angular/core';
import { TextInputModule } from './text-input/text-input.module';
import { ButtonModule } from './button/button.module';
import { TabelModule } from './tabel/tabel.module';
import { TableCheckboxModule } from './table-checkbox/table-checkbox.module';

const MODULES = [
  TextInputModule,
  ButtonModule,
  TabelModule,
  TableCheckboxModule,
  ButtonModule,
];

@NgModule({
  declarations: [],
  imports: [],
  exports: [...MODULES],
})
export class BaseComponentsModule {}
